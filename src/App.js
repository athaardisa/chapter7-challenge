import "./App.css";
import Header from "./components/header/Header";
import Service from "./components/main/Service";
import Section from "./components/main/Section";
import RentBanner from "./components/main/RentBanner";
import Faq from "./components/main/Faq";
import Footer from "./components/footer/Footer";
import Testimonial from "./components/main/Testimonial";

function App() {
  return (
    <div>
      <Header />
      <Service />
      <Section />
      <Testimonial />
      <RentBanner />
      <Faq />
      <Footer />
    </div>
  );
}

export default App;
