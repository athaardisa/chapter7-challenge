import React from "react";

import classes from './css/RentBanner.module.css'
import Button from '../UI/Button'

const RentBanner = () => {
  return (
    <div className="rent-banner pt-5" id="ourService">
      <div className="container px-lg-5 mt-5">
        <div className={classes.rentBox}>
          <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <Button className={classes.rentBtn} type="button">
            Mulai Sewa Mobil
          </Button>
        </div>
      </div>
    </div>
  );
};

export default RentBanner;
